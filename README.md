This is my Repository for the Web Programming homework for my semester marks.

The use of BitBucket was prescribed in the Homework's document.

We are using ASP.NET MVC 5 for the project.

My project is a Payments Management System for Freelancers. Basically, when a freelancer has several clients with each having different invoice balances, they [clients] can login and check their existing balance. This helps both the Freelancer and the Client to stay in sync.